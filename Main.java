import java.util.arrays;
import java.util.Scanner;



public class Main {
    public static void main (String args []) {
        Scanner sc = new Scanner(System.in);
        int input = sc.nextInt();
        String nama;
        int umur;
        int lama;
        Pegawai worker;

        Pegawai[] listPegawai = new Pegawai[input];
        for(int i=0; i<input;i++){
            nama = sc.next();
            umur = sc.nextInt();
            lama = sc.nextInt();

            worker = new Pegawai(nama, umur, lama);

            listPegawai[i] = worker;
        }
        System.out.println("Rata-rata gaji pegawai adalah " + rerataGaji(listPegawai));
        System.out.println("Gaji pegawai tertinggi adalah " + gajiTertinggi(listPegawai));
        System.out.println("Gaji pegawai terendah adalah " + gajiTerendah(listPegawai));
        sc.close();
    }
}

        