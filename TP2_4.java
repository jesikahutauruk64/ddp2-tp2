import java.util.Scanner;

public class TP2_4 {
    public static void main(String args[]) {
        Scanner inp = new Scanner(System.in);
        System.out.println("Selamat datang di desa Dedepedua");

        //userinputs

        int a = inp.nextInt();
        int b = inp.nextInt();
        int c = inp.nextInt();

        int maksimum = Math.max(a,Math.max(b, c));
        int minimum = Math.min(a,Math.min(b, c));

        if(((a*a) + (b*b) == (c*c)) || ((a*a) == (b*b) + (c*c)) || ((a*a) + (c*c) == (b*b))){
                    System.out.println("segitiga siku-siku, mereka penyusup");
                    if((a<maksimum) && (b<maksimum)){
                        System.out.println('b' + " adalah pemimpin mereka");
                    }
                    else if((b<maksimum) && (c<maksimum)){
                        System.out.println('c' + " adalah pemimpin mereka");
                    }
                    else if((c<maksimum) && (a<maksimum)){
                        System.out.println('a' + " adalah pemimpin mereka");
                    }
                       
                }
        else{
            System.out.println("bukan segitiga siku-siku, mereka bukan penyusup");
        }
    }
}