public class Pegawai {
    private String nama;
    private int umur;
    private int lamaBekerja;

    public Pegawai(String nama, int umur, int lamaBekerja){
        this.nama = nama;
        this.umur = umur;
        this.lamaBekerja = lamaBekerja;
    }

    public Pegawai(){

    }

    public double getGaji(){
        double gaji = 100;
        int kenaikan = lamaBekerja/3;
        for(int i = 0; i< kenaikan; i++){
            gaji = gaji*105/100;
        }
        if(umur>=40){
            gaji+=10;
        }
        return gaji;
    }

    public static double rerataGaji(Pegawai[] listPegawai){
        double total = 0;
        for(int i = 0; i<listPegawai.length;i++){
            total+=listPegawai[i].getGaji();
        }
        double rerata = total/listPegawai.length;
        return rerata;
    }

    public static String gajiterendah(Pegawai[] listPegawai){
        double temp = 0;
        String terendah = "";
        temp = listPegawai[0].getGaji();
        for(int i = 0; i<listPegawai.length;i++){
            if (temp>listPegawai[i].getGaji()){
                temp = listPegawai[i].getGaji();
                terendah = listPegawai[i].getNama();
            }
        }
        
        return terendah;
    }

    public static String gajitertinggi(Pegawai[] listPegawai){
        double temp = 0;
        String tertinggi = "";
        temp = listPegawai[0].getGaji();
        for(int i = 0; i<listPegawai.length;i++){
            if (temp<listPegawai[i].getGaji()){
                temp = listPegawai[i].getGaji();
                terendah = listPegawai[i].getNama();
            }
        }
        
        return tertinggi;
    }
    
}

 


